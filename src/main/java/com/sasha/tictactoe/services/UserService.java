package com.sasha.tictactoe.services;

import com.sasha.tictactoe.exceptions.UserIsInvalidException;
import com.sasha.tictactoe.exceptions.UserWithThatLoginExistsException;
import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.Role;
import com.sasha.tictactoe.model.Room;
import com.sasha.tictactoe.model.dto.AppUserDto;
import com.sasha.tictactoe.model.dto.AppUserRegisterDto;
import com.sasha.tictactoe.model.dto.LoginDto;
import com.sasha.tictactoe.repositories.RoleRepository;
import com.sasha.tictactoe.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Optional<AppUserDto> registerUser(AppUserRegisterDto user) {
        if (isInvalidUser(user)) {
            throw new UserIsInvalidException();
        }
        if (isLoginRegistered(user.getLogin())) {
            throw new UserWithThatLoginExistsException();
        }

        HashSet<Role> set = new HashSet<>();
        set.add(roleRepository.findById(1L).get());

        AppUser userToRegister = new AppUser(user.getLogin(), user.getPassword(), user.getName(), set);

        userToRegister.setPassword(bCryptPasswordEncoder.encode(userToRegister.getPassword()));
        userToRegister = userRepository.save(userToRegister);
        return Optional.ofNullable(AppUserDto.create(userToRegister));
    }

    private boolean isInvalidUser(AppUserRegisterDto user) {
        return user.getLogin() == null || user.getLogin().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty();
    }

    private boolean isLoginRegistered(String login) {
        return userRepository.findByLogin(login).isPresent();
    }

    @Override
    public Optional<AppUser> getAppUserWithId(Long identifier) {
        return userRepository.findById(identifier);
    }

    @Override
    public AppUser addToRoom(AppUser user, Room room) {
        user.setRoom(room);
        user = userRepository.save(user);
        return user;
    }

    @Override
    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) {
        // z bazy danych wyciągam użytkownika o loginie
        Optional<AppUser> userFromDB = userRepository.findByLogin(dto.getLogin());
        if (userFromDB.isPresent()) { // jeśli taki istnieje
            AppUser user = userFromDB.get(); // sprawdzam jego hasło (niżej)
            if (bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
                return userFromDB; // jeśli hasło się zgadza, to zwracam użytkownika
            }
        }
        return Optional.empty();
    }
}
