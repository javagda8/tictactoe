package com.sasha.tictactoe.services;

import com.sasha.tictactoe.model.Room;
import com.sasha.tictactoe.model.dto.ConnectToRoomDto;
import com.sasha.tictactoe.model.dto.RoomCreateDto;

import java.util.List;
import java.util.Optional;

public interface IRoomService {
    Optional<Room> createRoom(RoomCreateDto roomCreateDto);

    Optional<Room> getRoom(Long id);

    List<Room> getAllRooms();

    Optional<Room> connectToRoom(ConnectToRoomDto roomId);
}
