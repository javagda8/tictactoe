package com.sasha.tictactoe.services;

import com.sasha.tictactoe.exceptions.RoomExistsException;
import com.sasha.tictactoe.exceptions.WrongRoomNameException;
import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.Room;
import com.sasha.tictactoe.model.dto.ConnectToRoomDto;
import com.sasha.tictactoe.model.dto.RoomCreateDto;
import com.sasha.tictactoe.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService implements IRoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserService userService;

    @Override
    public Optional<Room> createRoom(RoomCreateDto roomCreateDto) {
        if (roomCreateDto == null || roomCreateDto.getName() == null
                || roomCreateDto.getName().isEmpty()) {
            throw new WrongRoomNameException();
        }
        if (roomExists(roomCreateDto.getName())) {
            throw new RoomExistsException();
        }

        Room newRoom = new Room();
        newRoom.setName(roomCreateDto.getName());

        newRoom = roomRepository.save(newRoom);

        return Optional.ofNullable(newRoom);
    }

    private boolean roomExists(String name) {
        return roomRepository.findByName(name).isPresent();
    }

    @Override
    public Optional<Room> getRoom(Long id) {
        return roomRepository.findById(id);
    }

    @Override
    public List<Room> getAllRooms() {
        return roomRepository.findAll();
    }

    @Override
    public Optional<Room> connectToRoom(ConnectToRoomDto connectDto) {
        Optional<Room> roomOptional = roomRepository.findById(connectDto.getRoomId());
        if (roomOptional.isPresent()) {
            Room room = roomOptional.get();

            Optional<AppUser> u = userService.getAppUserWithId(connectDto.getUserId());
            if(u.isPresent()){
                AppUser user = u.get();
                room.getConnectedUsers().add(user);

                roomRepository.save(room);
                userService.addToRoom(user, room);

                return Optional.ofNullable(room);
            }

        }
        return Optional.empty();
    }
}
