package com.sasha.tictactoe.services;

import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.Room;
import com.sasha.tictactoe.model.dto.AppUserDto;
import com.sasha.tictactoe.model.dto.AppUserRegisterDto;
import com.sasha.tictactoe.model.dto.LoginDto;

import java.util.Optional;

public interface IUserService {
    Optional<AppUserDto> registerUser(AppUserRegisterDto userToRegister);
    Optional<AppUser> getAppUserWithId(Long identifier);

    AppUser addToRoom(AppUser user, Room room);

    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto);
}
