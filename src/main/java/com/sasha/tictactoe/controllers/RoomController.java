package com.sasha.tictactoe.controllers;

import com.sasha.tictactoe.exceptions.RoomExistsException;
import com.sasha.tictactoe.exceptions.WrongRoomNameException;
import com.sasha.tictactoe.model.Room;
import com.sasha.tictactoe.model.dto.ConnectToRoomDto;
import com.sasha.tictactoe.model.dto.RoomCreateDto;
import com.sasha.tictactoe.response.ResponseFactory;
import com.sasha.tictactoe.services.IRoomService;
import com.sasha.tictactoe.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/room/")
public class RoomController {

    @Autowired
    private IRoomService roomService;

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Room> create(@RequestBody RoomCreateDto roomCreateDto) {
        try {
            Optional<Room> room = roomService.createRoom(roomCreateDto);

            if (room.isPresent()) {
                return ResponseFactory.created(room.get());
            }
        } catch (RoomExistsException e) {
            e.printStackTrace();
        } catch (WrongRoomNameException e) {
            e.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Room> get(@PathVariable(name = "id") Long id) {
        Optional<Room> room = roomService.getRoom(id);

        if (room.isPresent()) {
            return ResponseFactory.ok(room.get());
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<List<Room>> getAll() {
        List<Room> room = roomService.getAllRooms();

        return ResponseFactory.ok(room);
    }

    @RequestMapping(path = "/connect", method = RequestMethod.POST)
    public ResponseEntity<Room> connect(@RequestBody ConnectToRoomDto dto) {
        Optional<Room> optRoom = roomService.connectToRoom(dto);
        if (optRoom.isPresent()) {
            return ResponseFactory.ok(optRoom.get());
        }

        return ResponseFactory.badRequest();
    }

    // TODO: usuwanie pokoju
    // TODO: kontroler/service/repository dla GAME, w kontrolerze (analogicznie) metody add i get dla game
}
