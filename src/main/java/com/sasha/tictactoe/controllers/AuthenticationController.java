package com.sasha.tictactoe.controllers;

import com.sasha.tictactoe.exceptions.UserDoesNotExistException;
import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.Role;
import com.sasha.tictactoe.model.dto.AppUserDto;
import com.sasha.tictactoe.model.dto.AppUserRegisterDto;
import com.sasha.tictactoe.model.dto.AuthenticationDto;
import com.sasha.tictactoe.model.dto.LoginDto;
import com.sasha.tictactoe.response.ResponseFactory;
import com.sasha.tictactoe.services.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.sasha.tictactoe.configuration.JWTFilter.AUTHORITIES_KEY;
import static com.sasha.tictactoe.configuration.JWTFilter.SECRET;

@RestController
@CrossOrigin
@RequestMapping(path = "/auth/")
public class AuthenticationController {
    @Autowired
    private UserService userService;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody AppUserRegisterDto dto) {
        Optional<AppUserDto> user = userService.registerUser(dto);
        if (user.isPresent()) {
            return ResponseFactory.created(user.get());
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody LoginDto dto) {
        Optional<AppUser> appUserOptional = userService.getUserWithLoginAndPassword(dto);
        if (appUserOptional.isPresent()) {
            AppUser user = appUserOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            //We will sign our JWT with our ApiKey secret
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim("roles", translateRoles(user.getRoleSet()))
                    .signWith(signatureAlgorithm, signingKey)
                    .compact();
            return ResponseFactory.ok(new AuthenticationDto(user.getLogin(), user.getId(), token));
        }
        return ResponseFactory.badRequest();
    }

    private Set<String> translateRoles(Set<Role> roles) {
        return roles.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }
}
