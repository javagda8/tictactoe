package com.sasha.tictactoe.controllers;

import com.sasha.tictactoe.exceptions.UserErrorException;
import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.dto.AppUserDto;
import com.sasha.tictactoe.model.dto.AppUserRegisterDto;
import com.sasha.tictactoe.response.ResponseFactory;
import com.sasha.tictactoe.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/users/")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<AppUserDto> addUser(@RequestBody AppUserRegisterDto user, BindingResult result) {
        try {
            Optional<AppUserDto> registeredUser = userService.registerUser(user);
            if (registeredUser.isPresent()) {
                return ResponseFactory.created(registeredUser.get());
            }
        } catch (UserErrorException ex) {
            ex.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUser(@RequestParam(name = "id") Long id) {
        Optional<AppUser> user = userService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUserWithId(@RequestAttribute(name = "id") Long id) {
        Optional<AppUser> user = userService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

}
