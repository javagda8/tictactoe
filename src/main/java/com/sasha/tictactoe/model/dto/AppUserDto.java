package com.sasha.tictactoe.model.dto;

import com.sasha.tictactoe.model.AppUser;
import com.sasha.tictactoe.model.Room;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUserDto {
    private Long id;
    private String login;
    private String name;
    private Room room;

    public static AppUserDto create(AppUser user) {
        return new AppUserDto(
                user.getId(),
                user.getLogin(),
                user.getName(),
                user.getRoom());
    }
}
