package com.sasha.tictactoe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectToRoomDto {
    private long roomId;
    private long userId;
}
