package com.sasha.tictactoe.model;

import com.sun.xml.bind.v2.model.core.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime startTime;

    @OneToOne
    private AppUser playerLeft;
    @OneToOne
    private AppUser playerRight;

    @OneToMany(mappedBy = "game")
    private List<GameMove> moves;
}
