package com.sasha.tictactoe.repositories;

import com.sasha.tictactoe.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
