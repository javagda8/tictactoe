package com.sasha.tictactoe.components;

import com.sasha.tictactoe.model.Role;
import com.sasha.tictactoe.model.dto.AppUserRegisterDto;
import com.sasha.tictactoe.repositories.RoleRepository;
import com.sasha.tictactoe.services.IUserService;
import com.sasha.tictactoe.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    private IUserService userService;
    private RoleRepository roleRepository;

    @Autowired
    public DataInitializer(IUserService userService, RoleRepository roleRepository) {
        this.userService = userService;
        this.roleRepository = roleRepository;
        initalize();
    }

    private void initalize() {
        Role adminRole = new Role("ADMIN");
        roleRepository.save(adminRole);

        if (!userService.getAppUserWithId(1L).isPresent()) {
            userService.registerUser(new AppUserRegisterDto("admin", "admin", "admin"));
        }
    }
}
